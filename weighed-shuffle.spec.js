const seedrandom = require('seedrandom');
const weighedShuffle = require('./weighed-shuffle');

describe('Weighed Shuffle Function', () => {
  it('accepts an array of items and an array of weights and shuffles the items based on weights', () => {
    const originalRandom = Math.random;
    seedrandom('1', { global: true });

    const items = [1, 2, 3, 4, 5];
    const weights = [5, 4, 3, 2, 1];

    const result = weighedShuffle(items, weights);

    expect(result).toHaveLength(5);
    items.forEach(i => expect(result).toContain(i));
    expect(result).toMatchSnapshot();

    Math.random = originalRandom;
  });

  // Unskip this test and execute the tests many times to see the output in your console.
  // This should help "feel" what the algorithm does.
  test.skip('debug', () => {
    // we'll shuffle the integers from 1 to 1000
    const input = [...Array(1000).keys()].map(i => i + 1)

    // Weighs are "inverted" : first number will have weight 1000, second number will have weight 999
    // and so on until the last number that will have weight 1
    // This means the output array should "look like" the input.
    const weights = [...input].reverse()

    const result = weighedShuffle(input, weights);

    console.log(JSON.stringify(result));
  });
});
