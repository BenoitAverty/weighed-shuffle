const weighedShuffle = require('./weighed-shuffle')

describe("Weighed Shuffle Statistics", () => {
  test("Performance", () => {
    // we'll shuffle the integers from 1 to 1000
    const input = range(1000)

    // Weighs are "inverted" : first number will have weight 1000, second number will have weight 999
    // and so on until the last number that will have weight 1
    // This means the output array should "look like" the input.
    const weights = [...input].reverse()

    // number of times the algorithm is executed
    const iterations = 10000

    const t0 = performance.now();

    for (let n = 0; n < 10000; ++n) {
      const output = weighedShuffle(input, weights);
    }

    const t1 = performance.now();
    const timeInMs = t1 - t0;

    console.debug(`Executing the shuffle algorithm ${iterations} times took ${timeInMs}ms`);
    expect(timeInMs).toBeLessThanOrEqual(iterations * 1.5) // Maximum 1.5ms per shuffle on average.
  })

  test("Probabilities", () => {
    // we'll shuffle the integers from 1 to 100
    const input = range(100)

    // Weighs are "inverted" : first number will have weight 100, second number will have weight 99
    // and so on until the last number that will have weight 1
    // This means the output array should "look like" the input.
    const weights = [...input].reverse()

    // number of times the algorithm is executed
    const iterations = 100000

    // Execute algorithm many times and save the first item each time.
    const firstItems = []
    for (let n = 0; n < iterations; ++n) {
      const output = weighedShuffle(input, weights);
      firstItems.push(output[0])
    }

    // The probability to have the first item in the first position is 1000 / (1000 + 999 + ... + 1)
    // (equals to weight of first item divided by the sum of all weights)
    const probabilityOfFirstItemToBe1 = 100 / arraySum(weights)
    const measureFirstItemIs1 = firstItems.filter(i => i === 1).length / iterations
    expect(measureFirstItemIs1).toBeCloseTo(probabilityOfFirstItemToBe1)
  })
})

function arraySum(array) {
  return array.reduce((acc, curr) => acc + curr)
}

function range(n) {
  return [...Array(n).keys()].map(n => n + 1);
}