module.exports = weighedShuffle;

function weighedShuffle(items, weights) {
  const indices = [...Array(items.length).keys()].map(i => {
    const compKey = -Math.pow(Math.random(), 1.0 / weights[i]);

    return { index: i, compKey };
  });

  return indices.sort((a, b) => a.compKey - b.compKey).map(i => items[i.index]);
}
